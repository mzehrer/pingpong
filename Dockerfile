# We need to use the Rust build image, because
# we need the Rust compile and Cargo tooling
# FROM clux/muslrust as build
FROM rust:1.48 as build

# Creates a dummy project used to grab dependencies
RUN USER=root cargo new --bin pingpong
WORKDIR /pingpong

# Copies over *only* your manifests
COPY ./Cargo.lock ./Cargo.lock
COPY ./Cargo.toml ./Cargo.toml

# Builds your dependencies and removes the
# fake source code from the dummy project
RUN cargo build --release
RUN rm src/*.rs

# Copies only your actual source code to
# avoid invalidating the cache at all
RUN rm ./target/release/deps/pingpong*
COPY ./src ./src

# Builds again, this time it'll just be
# your actual source files being built
RUN cargo build --release

# Create a new stage with a minimal image
# because we already have a binary built
# FROM alpine:3.8
FROM debian:jessie-slim 

# Copies the binary from the "build" stage to the current stage
COPY --from=build pingpong/target/release/pingpong .
# COPY --from=build target/x86_64-unknown-linux-musl//release/pingpong .

# Configures the startup!
CMD ["./pingpong"]
