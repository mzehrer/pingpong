extern crate chrono;
extern crate hostname;
extern crate machine_ip;

use super::AppState;
use actix_web::{
    dev::HttpResponseBuilder, error, error::ResponseError, get, http::header, http::StatusCode,
    web, HttpResponse, Responder,
};

use std::io::Read;

use serde::Serialize;
use thiserror::Error;

use chrono::{DateTime, Utc};

#[derive(Serialize)]
struct Pong {
    name: String,
    address: String,
    time: String,
}

#[derive(Error, Debug)]
enum CustomError {
    #[error("Requested file was not found")]
    NotFound,
    #[error("Unknown Internal Error")]
    InternalError,
}
impl CustomError {
    pub fn name(&self) -> String {
        match self {
            Self::NotFound => "NotFound".to_string(),
            Self::InternalError => "InternalError".to_string(),
        }
    }
}
impl ResponseError for CustomError {
    fn status_code(&self) -> StatusCode {
        match *self {
            Self::NotFound => StatusCode::NOT_FOUND,
            Self::InternalError => StatusCode::INTERNAL_SERVER_ERROR,
        }
    }

    fn error_response(&self) -> HttpResponse {
        let status_code = self.status_code();
        let error_response = ErrorResponse {
            code: status_code.as_u16(),
            message: self.to_string(),
            error: self.name(),
        };
        HttpResponse::build(status_code).json(error_response)
    }
}

#[derive(Serialize)]
struct ErrorResponse {
    code: u16,
    error: String,
    message: String,
}

#[get("/boom")]
async fn boom(data: web::Data<AppState>) -> Result<&'static str, CustomError> {
    let log = &data.log;

    error!(log, "Triggering error");
    Err(CustomError::InternalError)
}

#[get("/ping")]
pub async fn ping(data: web::Data<AppState>) -> impl Responder {
    let log = &data.log;

    let now: DateTime<Utc> = Utc::now();
    info!(log, "local host time: {:?}", now.to_string());

    let hostname = hostname::get().unwrap();
    info!(log, "local host name: {:?}", hostname.to_string_lossy());

    let ip = machine_ip::get().unwrap();
    info!(log, "local ip address: {:?}", ip);

    let pong = Pong {
        name: hostname.to_string_lossy().to_string(),
        address: ip.to_string(),
        time: now.to_string(),
    };

    //HttpResponse::Ok().body("Hello world!")
    return web::Json(pong);
}
