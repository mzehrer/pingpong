extern crate envconfig;
extern crate envconfig_derive;
#[macro_use]
extern crate slog;
extern crate actix_slog;
extern crate actix_web;
#[macro_use]
extern crate failure;
extern crate reqwest;
#[macro_use]
extern crate serde_derive;
extern crate serde;
extern crate serde_json;

use actix_web::*;
mod handlers;

//use failure::Error;
//use std::fs::File;
//use std::io::prelude::*;
//use std::io::BufReader;

mod logging;
use actix_slog::StructuredLogger;

use envconfig::Envconfig;

#[derive(Envconfig)]
pub struct Config {
    #[envconfig(from = "API_KEY", default = "")]
    pub api_key: String,

    #[envconfig(from = "API_SECRET", default = "")]
    pub api_secret: String,
}

#[derive(Debug)]
pub struct AppState {
    log: slog::Logger,
}

#[actix_web::main]
async fn main() -> std::io::Result<()> {
    let config = match Config::init_from_env() {
        Ok(v) => v,
        Err(e) => panic!("Could not read config from environment: {}", e),
    };

    let log = logging::setup_logging();

    info!(log, "Server Started on localhost:8080");

    HttpServer::new(move || {
        let data = web::Data::new(AppState { log: log.clone() });
        App::new()
            .wrap(StructuredLogger::new(log.clone()))
            .app_data(data.clone())
            .service(handlers::ping)
            .service(handlers::boom)
    })
    .bind("0.0.0.0:8080")?
    .run()
    .await
}
