DOCKER = docker
REPO = zepan/pingpong
#REMOTEREPO = mzehrer/crofflr-imagegrinder
REMOTEREPO = registry.gitlab.com/mzehrer/pingpong

TAG = $(shell git rev-parse --abbrev-ref HEAD 2>/dev/null)
ifeq ($(TAG), master)
            TAG = latest
    else ifeq ($(TAG), HEAD)
            TAG = latest
    endif

$(info TAG is $(TAG))

all: build

run:
	docker run --rm -p 8080:8080 zepan/pingpong

shell:
	$(DOCKER) run -i -t $(REPO):$(TAG) /bin/bash

build:
	$(DOCKER) build -t $(REPO):$(TAG) .

nocache:
	$(DOCKER) build --no-cache -t $(REPO):$(TAG) .

push:
	$(DOCKER) tag $(REPO):$(TAG) $(REMOTEREPO):$(TAG)
	$(DOCKER) push $(REMOTEREPO):$(TAG)
